#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>

extern "C" {
  #include "user_interface.h"
}

#define WIFI_SSID "Danger Zone"
#define WIFI_PASS "nuggetdog"

#define MQTT_SERVER "192.168.0.155"
#define MQTT_PORT 1883
#define MQTT_TOPIC "kitchen/underCabinet/lights/4"
#define MQTT_USER "taft"
#define MQTT_PASS "nugget"
#define QOS 1

#define HOST_PREFIX   "esp-01_%s"
#define VER           "0.1.0"

static const uint8_t onBoardLED = 1;
static const uint8_t relayPin = 2;

char ESP_CHIP_ID[8];
char UID[16];
int networkRetries = 10;
int connectionCheckTime = 60;
int connectionCheckCount = 0;
bool sendStatus = false;
bool requestRestart = false;
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient, MQTT_SERVER, MQTT_PORT);

void setup() {
  // Pins
  pinMode(onBoardLED, OUTPUT);
  pinMode(relayPin, OUTPUT);

  // Turn off LED
  digitalWrite(onBoardLED, HIGH);

  // set mqtt callback
  mqttClient.set_callback(callback);

  // Connect to the network and send data
  if (networkSetup()) {
    blinkLED(3, 100);
  } else {
    blinkLED(10, 30);
  }
}

void loop() {
  mqttClient.loop();
  checkStatus();
  checkConnection();
}

bool networkSetup() {
  // Create unique id
  sprintf(ESP_CHIP_ID, "%06X", ESP.getChipId());
  sprintf(UID, HOST_PREFIX, ESP_CHIP_ID);
  
  // Connect to wifi
  WiFi.mode(WIFI_STA);
  WiFi.hostname(UID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  
  while ((WiFi.status() != WL_CONNECTED) && networkRetries--) {
    delay(500);
  }
  
  if (WiFi.status() == WL_CONNECTED) {
    delay(500);

    // Connect to mqtt broker
    while (!mqttClient.connect(MQTT::Connect(UID).set_keepalive(90).set_auth(MQTT_USER, MQTT_PASS)) && networkRetries--) {
      delay(1000);
    }
    
    if(mqttClient.connected()) {
      mqttClient.subscribe(MQTT_TOPIC);
      return true;
    }
  }
  
  return false;
}

void callback(const MQTT::Publish& pub) {
  if (pub.payload_string() == "toggle") {
    if (digitalRead(relayPin) == LOW) {
      digitalWrite(onBoardLED, LOW);
      digitalWrite(relayPin, HIGH);
    } else {
      digitalWrite(onBoardLED, HIGH);
      digitalWrite(relayPin, LOW);
    }
  } else if (pub.payload_string() == "on") {
    digitalWrite(onBoardLED, LOW);
    digitalWrite(relayPin, HIGH);
  } else if (pub.payload_string() == "off") {
    digitalWrite(onBoardLED, HIGH);
    digitalWrite(relayPin, LOW);
  } else if (pub.payload_string() == "reset") {
    requestRestart = true;
  }
  sendStatus = true;
}

void checkStatus() {
  if (sendStatus) {
    sendReport();
    sendStatus = false;
  }
  
  if (requestRestart) {
    blinkLED(3, 300);
    ESP.restart();
  }
}

// Send sensor data
void sendReport() {
  if(digitalRead(relayPin) == HIGH)  {
        mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/stat", "on").set_qos(QOS));
    } else {   
        mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/stat", "off").set_qos(QOS));
    }
}

void checkConnection() {
  connectionCheckCount++;
  if (connectionCheckCount > connectionCheckTime) {
    connectionCheckCount = 0;
    if (WiFi.status() == WL_CONNECTED)  {
      if (!mqttClient.connected()) {
        requestRestart = true;
      }
    } else {
      requestRestart = true;
    }
  }
}

// Blinks the LED a number of times
void blinkLED(int blinks, int duration) {             
  for(int i = 0; i < blinks; i++) {
    digitalWrite(onBoardLED, LOW);
    delay(duration);
    digitalWrite(onBoardLED, HIGH);
    delay(duration);
  }
}
